﻿// Copyright messages etc.

using System;
using System.IO;

namespace SingerSort
{
    class Program
    {
        /// <summary>
        /// Main
        /// Entry point to the Singer Sort Excercise application
        /// Uses the Classes ClassSingerSort and ClassDuration in the same namespace of SingerSort
        /// </summary>
        /// <param name="args">Option -b to specify to use bubble sort rather than the built in sort</param>
        static void Main(string[] args)
        {
            ClassSingerSort SingerSort = new ClassSingerSort();

            // Check to see if -b has been entered on the command line to use the bubble sort
            if (args.Length > 0)
            {
                if (args[0] == "-b")
                {
                    SingerSort.UseBubbleSort = true;
                }
            }
            // Show a welcome message to the user
            Console.WriteLine("---- Singer Sort Exercise V1.0 ----");
            Console.WriteLine();
            Console.WriteLine("After the prompt 'sort>' please enter a string to sort");
            Console.WriteLine("Or the command file=xxxx where xxxx is the path to the file to sort");
            Console.WriteLine("Or hit return to exit the program.");
            String UserCommand = "";
            for (; ; )          // Wait for the user to e
            {
                Console.WriteLine();
                Console.Write("sort>");
                UserCommand = Console.ReadLine();
                if (UserCommand.Length == 0)           // Exit the application on an empty line
                {
                    break;
                }
                else
                {       // User has entered a line

                    if (SingerSort.CheckUserCommandForFile(UserCommand)) // Check that the user has specified a source file 
                    {
                        if (!SingerSort.CheckFileToSortExists())        // Report an error if the source file does not exist
                        {
                            Console.WriteLine($"File {SingerSort.FileToSort} does not exist");
                            continue;
                        }
                        else
                        {
                            // Get the Destination File name from the user.
                            string DestinationFilename = GetDestinationFileName(SingerSort.DestinationFilename);
                            if (DestinationFilename.Length == 0)  // If nothing entered go back to prompt 
                            {
                                continue;
                            }
                            SingerSort.DestinationFilename = DestinationFilename;
                        }

                    }
                    string Results = SingerSort.Sort(UserCommand);                      // Run the sort
                    Console.WriteLine(Results);                                         // Display the results
                }
            }
        }

        /// <summary>
        /// GetDestinationFileName
        /// Get the name of the file to place the sorted results into
        /// If the file exists ask the user if they want to over write it
        /// Stay in a loop until the answer y or n or hit Esc or control-C
        /// </summary>
        /// <param name="DestinationFilename">Default name of the file to place the sorted results into</param>
        /// <returns>The name of file to place the sorted results into</returns>
        static string GetDestinationFileName(string DestinationFilename)
        {
            for (; ; )
            {
                Console.WriteLine($" Please enter the name of the destination file or enter return for {DestinationFilename}");
                String UserInput = Console.ReadLine();
                if (UserInput.Length != 0)
                {
                    DestinationFilename = UserInput;
                }
                if (File.Exists(DestinationFilename))
                {
                    Console.WriteLine($" {DestinationFilename} exists. Overwrite ? y/n or Esc to cancel");
                    ConsoleKey KeyHit = Console.ReadKey().Key;
                    Console.WriteLine();
                    if (KeyHit == ConsoleKey.Escape)
                    {
                        return "";
                    }
                    if (KeyHit == ConsoleKey.Y)
                    {
                        return (DestinationFilename);
                    }
                }
                else
                {
                    return DestinationFilename;
                }
            }
        }
    }
}

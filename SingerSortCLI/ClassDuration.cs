﻿// Copyright messages etc.
using System;

namespace SingerSort
{
/// <summary>
/// ClassDuration
/// Simple Class to return the duration between a start and finish time in milliseconds
/// </summary>
   public  class ClassDuration
    {
        private DateTime Started;
        private DateTime Finished;

        /// <summary>
        /// Record the Start time
        /// </summary>
        public void Start()
        {
            this.Started = DateTime.UtcNow;
        }
        /// <summary>
        /// Record the finish time
        /// </summary>
        public void Finish()
        {
            this.Finished = DateTime.UtcNow;
        }

        /// <summary>
        /// Calculate the duration betyween the start and finish time in milliseconds
        /// </summary>
        /// <returns></returns>
        public double GetDuration()
        {
            double duration = (this.Finished - this.Started).TotalMilliseconds;
            return duration;
        }

    }
}

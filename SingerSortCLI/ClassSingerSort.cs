﻿// Copyright messages etc.
using System;
using System.IO;

namespace SingerSort
{
    /// <summary>
    /// Class ClassSingerSort
    /// Provide the Sorting functions for the Singer Instruments Ltd Sorting Excercise
    /// The class is designed to work both with a command line version of the application
    /// as well as a windows forms based application
    /// </summary>
    public class ClassSingerSort
    {

        private string StringToSort = "";
        private string SortedString = "";
        public bool UseBubbleSort = false;                   // If true use the hand coded bubble sort
                                                            // Otherwise use the DOT NET Framework Array.Sort
        public string DestinationFilename { get; set; } = "SORTRESULTS.TXT";
        public string FileToSort = "";
        public double StringSortDuration = 0.0;             // How long it took to sort the string
        public double FileReadAndSortDuration = 0.0;        // How long it took to read the file and sort the string
        public double FileWriteDuration = 0.0;              // How long it took to write the output file
        public string ReportResults = "";                   // Messages to report to user
        public string ErrorMessages = "";                   // Accumulated error messages
        public bool Error = false;                          // True if an error or exception occured during processing
        /// <summary>
        /// ClearResults()
        /// Clear Results and Error properties prior to running a sort as the class may be used multiple times
        /// for multiple different sorts
        /// </summary>
        private void ClearResults()
        {
            this.ReportResults = "";
            this.SortedString = "";
            this.StringSortDuration = 0.0;
            this.FileReadAndSortDuration = 0.0;
            this.FileWriteDuration = 0.0;
            this.ErrorMessages = "";
            this.Error = false;
        }

        /// <summary>
        /// CheckUserCommandForFile
        /// Check the command line entered by the user to see if it is specifying a file
        /// </summary>
        /// <param name="StringToSort">The command line entered by the user</param>
        /// <returns>Return true if the command line contains file=. Otherwise return false</returns>
        public bool CheckUserCommandForFile(string StringToSort = "sortedresults.txt")
        {
            if (StringToSort.StartsWith("file=", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                this.FileToSort = StringToSort.Substring(5);
                return true;
            }
            return false;
        }
        /// <summary>
        /// CheckFileToSortExists
        /// </summary>
        /// <returns>True if the FileToSort property contains the name of a file that exists. Otherwise return false.</returns>
        public bool CheckFileToSortExists()
        {
            if (File.Exists(this.FileToSort))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sort
        /// Process the command line entered by the user
        /// And select to eother sort the string or sort the file
        /// Returns a report to show the user
        /// Note that the Error Property will be true if an error was detected
        /// </summary>
        /// <param name="StringToSort">The command line entered by the user</param>
        /// <returns>Results to show to user.</returns>
        public string Sort(string StringToSort = "")
        {
            string Results = "";
            ClearResults();
            if (this.CheckUserCommandForFile(StringToSort))         // If a file is specified call FileSort
            {
                string FileToSort = StringToSort.Substring(5);
                bool FileSortResult = this.FileSort(FileToSort.Trim());
                if (FileSortResult)
                {
                    Results = this.ReportResults;      // Return results to show user
                }
                else
                {
                    Results = "Error\n";
                    Results = this.ErrorMessages + "\n"; // Return Errors detected
                }
                return Results;
            }
            else                                                    // Else Sort the string
            {
                string SortedString = this.StringSort(StringToSort);
                Results += "SortedString:\n";
                Results += $"{SortedString}\n\n";
                Results += $"The time taken to sort the string was {this.StringSortDuration}ms\n";
                Results += $"Input String Length = {StringToSort.Length}\n";
                Results += $"Sorted String Length = {SortedString.Length}\n";
                if(this.UseBubbleSort)
                    {
                    Results += "Using the bubble sort.\n";
                    }
                return Results;
            }

        }

        /// <summary>
        /// StringSort
        /// Sort the string in the StringToSort argument
        /// If this.UseBubbleSort is true use the hand coded bubble sort
        /// Otherwise use Array.Sort from the .NET framework 
        /// Using the Array.Sort method provided in the .NET framework
        /// </summary>
        /// <param name="StringToSort"></param>
        /// <returns>The sorted string</returns>
        public string StringSort(string StringToSort)
        {
            ClassDuration Duration = new ClassDuration();
            this.StringToSort = StringToSort;
            Duration.Start();
            char[] ToBeSorted = StringToSort.ToCharArray();
            if(this.UseBubbleSort)          // Decide which sort to use
                {
                this.BubbleSort(ToBeSorted);
                }
            else
                {
                Array.Sort<char>(ToBeSorted); ;
                }
            char[] Sorted = this.RemoveUnwantedCharactersFromSortedString(ToBeSorted);
            Duration.Finish();
            this.SortedString = new string(Sorted);
            this.StringSortDuration = Duration.GetDuration();
            return this.SortedString;
        }

        

        public void BubbleSort(char[] ToBeSorted)
        {
            int len = ToBeSorted.Length;
            char temp;
            for(int j=0;j<=len-2;j++)
                {
                for(int i=0;i<=len-2;i++)
                    {
                    if(ToBeSorted[i]>ToBeSorted[i+1])
                        {
                        temp = ToBeSorted[i + 1];
                        ToBeSorted[i + 1] = ToBeSorted[i];
                        ToBeSorted[i] = temp;
                        }
                    }
                }
        }



        /// <summary>
        /// FileToSort
        /// Sort the bytes in the specified file into ascending order and place the results
        /// in the file specified in this.DestinationFilename
        /// </summary>
        /// <param name="FileToSort">The full path to the file to sort</param>
        /// <returns>False on error</returns>
        public bool FileSort(string FileToSort)
        {
            bool ReturnResult = true;                               // Return true if OK otherwise false for error
            if (File.Exists(FileToSort))
            {
                try
                {
                    ClassDuration Duration = new ClassDuration();
                    Duration.Start();
                    string FileContents = File.ReadAllText(FileToSort);             // Read the file contents
                    string SortedString = this.StringSort(FileContents);            // Call the String sort method
                    try
                    {
                        Duration.Finish();
                        this.FileReadAndSortDuration = Duration.GetDuration();      // File Read and sort duration
                        Duration.Start();
                        File.WriteAllText(this.DestinationFilename, SortedString);  // Write the output file - may get an exception
                        Duration.Finish();
                        this.FileWriteDuration = Duration.GetDuration();            // File Write duration
                        long InputFileSize = this.GetFileSize(this.FileToSort);
                        long OutputFileSize = this.GetFileSize(this.DestinationFilename);
                        // Report results to user
                        this.ReportResults = $"The input file was {FileToSort} of length {InputFileSize} bytes.\n";
                        if (OutputFileSize >= 0L)
                        {
                            this.ReportResults += $"The sorted results were written to file {this.DestinationFilename}";
                            this.ReportResults += $" of size {OutputFileSize} bytes.\n";
                        }
                        else                //For some reason the output file could not be found
                        {
                            this.ReportResults += $"The sorted results file {this.DestinationFilename} could not be found.";
                        }
                        this.ReportResults += $"The sort took {this.StringSortDuration.ToString()}ms.\n";
                        this.ReportResults += $"The sort including accessing the file took {this.FileReadAndSortDuration.ToString()}ms.\n";
                        this.ReportResults += $"Writing the sort results to {this.DestinationFilename} took  {this.FileWriteDuration.ToString()}ms.\n";
                    }
                    catch (Exception Ex)
                    {
                        this.ErrorMessages += $"Error: The File {this.DestinationFilename} could not be written to. ({Ex.Message})\n";
                        this.Error = true;
                        ReturnResult = false;
                    }
                }
                catch (Exception Ex)
                {

                    this.ErrorMessages += $"Exception reading file {FileToSort} . ({Ex.ToString()})\n";
                    this.Error = true;
                    ReturnResult = false;
                }
            }
            else                    // The input file to sort could not be found
            {

                this.ErrorMessages = $"{FileToSort} does not exist";
                this.Error = true;
                ReturnResult = false;
            }



            return ReturnResult;
        }

        /// <summary>
        /// RemoveUnwantedCharactersFromSortedString
        /// Removes unwanted character from the character array that has already been sorted
        /// All characters < 0x20 and > 0x7F are unwanted
        /// As the character array has already been sorted only the begining (<0x20) and the end (>0x7f) 
        /// of the array needs to be checked.
        /// </summary>
        /// <param name="InString"></param>
        /// <returns></returns>
        private char[] RemoveUnwantedCharactersFromSortedString(char[] InString)
        {
            char[] OutString = { };
            int StartOfFilteredString = 0;
            int EndOfFilteredString = 0;
            int NoOfChars = InString.Length;
            if (NoOfChars > 0)
            {
                try
                {
                    // Find first character in the sorted string that is valid
                    for (int i = 0; i < NoOfChars; i++)
                    {
                        if (InString[i] >= ' ')
                        {
                            StartOfFilteredString = i;
                            break;
                        }
                    }
                    // Find last character in the sorted string that is valid
                    for (int i = NoOfChars - 1; i >= 0; i--)
                    {
                        if (InString[i] <= Convert.ToChar(0x7f))
                        {
                            EndOfFilteredString = i;
                            break;
                        }
                    }
                    if (StartOfFilteredString <= EndOfFilteredString)
                    {
                        int ResultLength = EndOfFilteredString - StartOfFilteredString + 1;
                        OutString = new char[ResultLength];
                        Array.Copy(InString, StartOfFilteredString, OutString, 0, ResultLength);
                    }
                }
                catch (Exception ex)
                {

                    this.ErrorMessages = $"Exception processing string : {ex.ToString()}";
                    this.Error = true;
                }
            }
            else
            {
                this.ErrorMessages = $"Empty String";
            }

            return OutString;
        }

        /// <summary>
        /// GetFileSize
        /// Simple function to return the file size or -1L on an exception
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public long GetFileSize(string FileName)
        {
            long FileSize = 0L;
            try
            {
                FileInfo fi = new FileInfo(FileName);
                FileSize = fi.Length;
            }
            catch (Exception Ex)
            {
                FileSize = -1L;
            }
            return FileSize;
        }

    }
}

// Copyright and other messages etc.

using NUnit.Framework;
using System.Threading;
using System.IO;
using System.Reflection;
/// <summary>
/// A selection of unit tests for the Singer Sorting Exercise using Nunit
/// </summary>
namespace SingerSort
{
    public class Tests
    {

    /// <summary>
    /// Setup ready for tests
    /// Nothing needed at the moment as the test files are taken from the application bin folders
    /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Return the path to the application executable folder to access the test files
        /// </summary>
        /// <returns></returns>
        public string GetApplicationFolder()
        {
            var ApplicationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            return ApplicationFolder.Replace("file:\\", "");

        }

        /// <summary>
        /// Return true if all the characters in the string are sorted in an ascending order
        /// </summary>
        /// <param name="SortedString"></param>
        /// <returns></returns>
        public bool CheckCharsAreSorted(string SortedString)
        {
            char[] Characters = SortedString.ToCharArray();
            int length = Characters.Length;
            if (length>0)
                {
                char LastChar = '\0';
                for(int i=0;i<length;i++)
                    {
                    if(Characters[i]<LastChar)
                        {
                        return false;
                        }
                    LastChar = Characters[i];
                    }
                }
                return true;
        }

        /// <summary>
        /// Test that CheckCharsAreSorted is working
        /// </summary>
        [Test]
        public void Test_CheckCharsAreSorted()
        {
            bool result = this.CheckCharsAreSorted("abcdefg");
            Assert.True(result, "CheckCharsAreSorted is not working");
        }

        /// <summary>
        /// Test that CheckCharsAreSorted is working
        /// </summary>
        [Test]
        public void Test_CheckCharsAreSorted2()
        {
            bool result = this.CheckCharsAreSorted("123450678");
            Assert.False(result, "CheckCharsAreSorted is not working");
        }

        /// <summary>
        /// Test that the Class Duration records duration correctly
        /// </summary>
        [Test]
        public void Test_Duration_2000()
        {
            ClassDuration D = new ClassDuration();
            D.Start();
            Thread.Sleep(2100);
            D.Finish();
            Assert.True(D.GetDuration() > 2000, "Should be greater than 2 seconds");
        }

        /// <summary>
        /// Test String Sort
        /// </summary>
        [Test]
        public void Test_StringSort()
        {

            ClassSingerSort S = new ClassSingerSort();
            string Result = S.StringSort("q1w2e3!r�4t%5y^6u&7i*8o(9)p");
            Assert.AreEqual(Result, "!%&()*123456789^eiopqrtuwy");

        }

        /// <summary>
        /// Test String Sort results are sorted
        /// </summary>
        [Test]
        public void Test_StringSort_IsSorted()
        {

            ClassSingerSort S = new ClassSingerSort();
            string Result = S.StringSort("q1w2e3!r�4t%5y^6u&7i*8o(9)p");
            bool CheckSorted = this.CheckCharsAreSorted(Result);
            Assert.True(CheckSorted, "The results are not correctly sorted");
        }

        /// <summary>
        /// Test the method CheckFileToSort Exists for present file
        /// </summary>

        [Test]
        public void Test_FileDoesExist()
        {
            string TestFile = this.GetApplicationFolder() + "\\example.txt";
            ClassSingerSort S = new ClassSingerSort();
            S.FileToSort = TestFile;
            bool result = S.CheckFileToSortExists();
            Assert.IsTrue(result,$"File {TestFile} not found");
        }

        /// <summary>
        /// Test the method CheckFileToSort Exists for missing file
        /// </summary>
        [Test]
        public void Test_FileDoesNotExist()
        {
            string TestFile = this.GetApplicationFolder() + "\\nonexistantfile.txt";
            ClassSingerSort S = new ClassSingerSort();
            S.FileToSort = TestFile;
            bool result = S.CheckFileToSortExists();
            Assert.IsFalse(result,$"Reported that file {TestFile} exists which it should not");
        }


        /// <summary>
        /// Test File Sort
        /// Test passes if the function returns true
        /// </summary>
        [Test]
        public void Test_FileSort()
        {

            ClassSingerSort S = new ClassSingerSort();
            string TestFile = this.GetApplicationFolder() + "\\example.txt";
            bool Result = S.FileSort(TestFile);
            Assert.IsTrue(Result,$"Filesort of {TestFile} failed");
        }

    }
}